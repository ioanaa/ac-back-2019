const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,varsta VARCHAR(3),cnp VARCHAR(20),data_nastere VARCHAR(10),sex VARCHAR(2))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.abonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta=req.body.varsta;
    let cnp=req.body.cnp;
    let DataNastere = req.body.DataNastere;
    let error = [];

    console.log(req.body.DataNastere)
    if (!nume||!prenume||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv||!varsta||!cnp||!DataNastere) {
        error.push("Unul sau mai multe campuri nu au fost introduse");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
    } else { 
        if (nume.length < 3 || nume.length > 20) {
            console.log("Nume invalid!");
            error.push("Nume invalid");
        
        } else if (!nume.match("^[A-Za-z]+$")) {
            console.log("Numele trebuie sa contina doar litere!");
            error.push("Numele trebuie sa contina doar litere!");
        }
        if (prenume.length < 3 || prenume.length > 20) {
            console.log("Prenume invalid!");
            error.push("Prenume invalid!");
        } else if (!prenume.match("^[A-Za-z]+$")) {
            console.log("Prenumele trebuie sa contina doar litere!");
            error.push("Prenumele trebuie sa contina doar litere!");
        }
        if (telefon.length != 10) {
            console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
            error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
        } else if (!telefon.match("^[0-9]+$")) {
            console.log("Numarul de telefon trebuie sa contina doar cifre!");
            error.push("Numarul de telefon trebuie sa contina doar cifre!");
        }
        if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
            console.log("Email invalid!");
            error.push("Email invalid!");
        }
        if (!facebook.includes("https://www.facebook.com/") && !facebook.includes("https://web.facebook.com/") ) {
            console.log("Facebook invalid!");
            error.push("Facebook invalid!");
        }
        if(nrCard.length !=16){
            console.log("Cardul trebuie sa fie de 16 cifre!");
            error.push("Cardul trebuie sa fie de 16 cifre!");
        } else if (!nrCard.match("^[0-9]+$")) {
            console.log("Cardul trebuie sa contina doar cifre!");
            error.push("Cardul trebuie sa contina doar cifre!");
        }
        if(cvv.length !=3){
            console.log("Cvv trebuie sa fie de 3 cifre!");
            error.push("Cvv trebuie sa fie de 3 cifre!");
        } else if (!cvv.match("^[0-9]+$")) {
            console.log("Cvv trebuie sa contina doar cifre!");
            error.push("Cvv trebuie sa contina doar cifre!");
        }
        if (varsta.length <1 || varsta.length>3) {
            console.log("Varsta trebuie sa aiba cel putin o cifra si cel mult 3!");
            error.push("Varsta trebuie sa aiba cel putin o cifra si cel mult 3!");
        } else if (!varsta.match("^[0-9]+$")) {
            console.log("Varsta trebuie sa contina doar cifre!");
            error.push("Varsta trebuie sa contina doar cifre!");
        }
        if (cnp.length != 13) {
            console.log("CNP-ul trebuie sa fie de 13 cifre!");
            error.push("CNP-ul trebuie sa fie de 13 cifre!");
        } else if (!cnp.match("^[0-9]+$")) {
            console.log("CNP-ul trebuie sa contina doar cifre!");
            error.push("CNP-ul trebuie sa contina doar cifre!");
        }
     // In continuare este exercitiul 6  
     var sex;
    if(cnp[0]==1||cnp[0]==3||cnp[0]==5) 
       sex='M';
    if(cnp[0]==2||cnp[0]==4||cnp[0]==6)
       sex='F';    

    var an=String((cnp[1]))+String(cnp[2]);
    var luna=String((cnp[3]))+String(cnp[4]);
    var zi=String((cnp[5]))+String(cnp[6]);
      
           if(an<="19")
           an="20"+an;
           else an="19"+an;
       
        var luna=(String((cnp[3]))+String(cnp[4]));   
        var  zi=String((cnp[5]))+String(cnp[6]);
       
    function validare_forma_data( data_de_nastere)
    {
        var ok =1;
            if(data_de_nastere.length!=10) return 0;
          for(var i=0; i<data_de_nastere.length;i++){
            if(i==4||i==7) 
            {
                if(data_de_nastere[i]!="-") ok=0;
            }
            else {
                if(!data_de_nastere[i].match("^[0-9]+$"))
                 ok=0;
                }
               
           }
           return ok;
        }

        if(validare_forma_data(DataNastere)==0)
       { 
           error.push("Data nu are un formatul corect!");
        
       }
        
        // validare legatura dintre data din cnp si data de nastere introdusa
        if(DataNastere!=(an+"-"+luna+"-"+zi))
        error.push("Data introdusa nu este aceeasi cu cea din cnp!");
            
         }
    
    if (error.length === 0) {
 // Aici am asigurat unicitatea campului email
        const interogare ="SELECT COUNT(nume) as countNumber FROM `abonament` WHERE email='"+email+"'";
    
        connection.query(interogare,  function(err, result){

                        if(result[0].countNumber > 0)    {

                        console.log("S a intrat cu email-ul "+ email +" care apare de " + result[0].countNumber + "");
                        return res.status(400).send({
                         message: 'Emailul exista deja!'});
                                                         }
                        else {
                        const sql =
              "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,data_nastere,sex) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','"+facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"','" + varsta+"','"+ cnp+"','"+ DataNastere+"','"+sex+"')";
            connection.query(sql, function(err, result)       {
                    if (err) throw err;
                    else res.status(200).send("Abonament achizitionat!")
                    console.log("S-a facut inserarea"); 
                                                              });
                            }
            });
                          }                       
            else {
              res.status(500).send(error);
              console.log("Eroare la inserarea in baza de date!");
            }
            
});